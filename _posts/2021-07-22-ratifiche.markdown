---
title:  "Ratifi-che?! chi!?"
date:   2021-07-22 08:00:00 +0100
layout: single

tagline: "La ratifica del Collegio Docenti degli scrutini non ha senso, è inutile e forse completamente illegittima"

header:
  teaser: /assets/images/2021-07-22-ratifiche-header.jpg
  overlay_image: /assets/images/2021-07-22-ratifiche-header.jpg
  overlay_filter: 0.5

categories: blog
tags : normative
---


Da un po' di anni sta andando di moda sottoporre a ratifica del Collegio dei Docenti i risultati degli scrutini finali, quando anche quelli intermedi.

Non si capisce da dove sia nata questa superflua abitudine. 

Infatti basta riferirsi alle leggi che regolano la vita scolastica per capire che è un prassi priva di ogni fondamento normativo.

"Negli istituti e scuole di istruzione secondaria  superiore,  le 
competenze relative alla valutazione periodica e finale degli  alunni
spettano al consiglio di classe ..." ( D.Lgs 297/94 -art 5 c.7 - "Testo Unico sulla scuola" )

Mentre il Collegio dei Docenti "valuta periodicamente l'andamento complessivo dell'azione didattica .." ( D.Lgs 297/94 -art 7 c. 2a - "Testo Unico sulla scuola" ) 

Valutare è cosa ben diversa da ratificare. 

Come dice il vocabolario Treccani ratificare, dal latino medievale ratum facĕre, significa rendere valido e in senso strettamente giuridico approvare, facendolo proprio, un atto, un negozio, un contratto compiuto da altri.

Quindi il Collegio non deve ratificare alcunché, ai Dirigenti che non dovessero ancora capire gli si può contestare perché permettano la pubblicazione dei risultati prima di questo importante atto!

Certamente il collegio è tenuto a prendere visione e fare un analisi dei risultati al fine di migliorare l'azione didattica complessiva. 
Ma questa è cosa ben diversa da ratificare! Su una presa visione non si vota !

Poi non si capisce perché un organo collegiale debba far proprie delle decisioni di un altro, con le conseguenti assunzioni di responsabilità. 
Non essendo tra l'altro a conoscenza degli elementi essenziali che hanno mosso i Consigli nelle loro delibere: voti, progressioni degli apprendimenti eccetera.

Per dare un tono di maggior rigore formale agli scrutini?  
Ma assicurare che uno scrutinio, al pari di ogni delibera collegiale, sia svolto nel pieno rigore del dettato normativo è compito proprio del presidente: il Dirigente Dcolastico!
 
Non è che dietro queste richieste si nasconde la voglia di allargare la platea dei responsabili? A pensar male si fa peccato ...

Siccome non sono un cattivo pensatore devo pensare altro.  
Ignoranza? Forse.  
Consuetudine? Forse.  
C'e da dire che i Dirigenti sono quasi tutti iscritti ad una associazione sindacale che si comporta più come un associazione datoriale che vede la controparte non nell'ARAN o Ministero ma nei docenti!  
È quindi tutto un fiorire di consigli, strategie per far scivolare lontan da sé, almeno in parte, le responsabilta e addossarle invece ai poveri docenti. 

Basta trovare un idea in questo senso perché si propaghi in modo virale tra dirigenti, non trovando alcuna opposizione da parte di mansueti docenti. Che forse per quieto vivere o perché non fanno gli avvocati, lasciano passare tutte queste cose senza neanche provare a difendere le loro prerogative, la loro dignità professionale e a quanto pare neanche per difesa della logica e della semantica.

Costringere i docenti a fare delle cose inutili e non dovute non credo sia il compito di un Dirigente di un Istituzione posta a fondamento della Democrazia dalla nostra Costituzione. Eppure sembra il passatempo preferito di tanti che sembrano godere di questo puro e gratuito esercizio di potere.

UPDATE: a chi non bastasse quanto scritto può forse essere utile il pensiero, ancor più severo, di questo ex Provveditore agli studi e Dirigente scolastico che addirittura ritiene 
[illegittima la ratifica ...](https://www.scuolaeamministrazione.it/it/scrutinio-finale-e-ratifica-collegiale/) [(arch.)](https://web.archive.org/web/20210625212001/https://www.scuolaeamministrazione.it/it/scrutinio-finale-e-ratifica-collegiale/)

